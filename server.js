import express from 'express';
import 'babel-polyfill';
import cors from 'cors';
import env from './env';

import routes from "./app/routes/index";


const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE");
    next();
  });
app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use('/', routes);

app.listen(env.port).on('listening', () => {
    console.log(`server listening on port ${env.port}`);
})