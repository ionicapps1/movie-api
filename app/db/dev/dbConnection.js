import pool from "./pool";
//var pool = require('./pool');

pool.on("connect", () => {
  console.log("connected to the db");
});

/**
 * Create User Table
 * CREATE TABLE test
  (id SERIAL PRIMARY KEY, 
  name VARCHAR(100) UNIQUE NOT NULL, 
  phone VARCHAR(100));
 */
const createMoviesTable = () => {
  const movieCreateQuery = `CREATE TABLE IF NOT EXISTS movies
  (id SERIAL PRIMARY KEY, 
  popularity DECIMAL, 
  vote_count INTEGER, 
  video BOOLEAN, 
  poster_path VARCHAR(150),
  adult BOOLEAN,
  backdrop_path VARCHAR(150),
  original_language VARCHAR(20),
  original_title VARCHAR(100),
  genre_ids INTEGER[],
  title VARCHAR(100),
  vote_average DECIMAL,
  overview VARCHAR(500),
  release_date DATE)`;

  console.log("creating table");
  pool
    .query(movieCreateQuery)
    .then((res) => {
      console.log("then");
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log("catch");
      console.log(err);
      pool.end();
    });
};

/**
 * Drop User Table
 */
const dropMovieTable = () => {
  const moviesDropQuery = "DROP TABLE IF EXISTS movies";
  pool
    .query(moviesDropQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};

/**
 * Create All Tables
 */
const createAllTables = () => {
  createMoviesTable();
};

/**
 * Drop All Tables
 */
const dropAllTables = () => {
  dropMovieTable();
};

pool.on("remove", () => {
  console.log("client removed");
  process.exit(0);
});

export { createAllTables, dropAllTables };

require("make-runnable");
