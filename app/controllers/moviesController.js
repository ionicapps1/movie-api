import { errorMessage, successMessage, status } from "../helpers/status";

import { isEmpty } from "../helpers/validation";

import dbQuery from "../db/dev/dbQuery";

const addMovie = async (req, res) => {
  console.log('adding a new moovie')
  const {
    popularity,
    vote_count,
    video,
    poster_path,
    adult,
    backdrop_path,
    original_language,
    original_title,
    genre_ids,
    title,
    vote_average,
    overview,
    release_date,
  } = req.body;

  if (
    isEmpty(poster_path) ||
    isEmpty(original_title) ||
    isEmpty(title) ||
    isEmpty(overview)
  ) {
    errorMessage.error =
      "poster_path, original_title, title and overview field cannot be empty";
      console.log('error')
    return res.status(status.bad).send(errorMessage);
  }

  const createMovieQuery = `INSERT INTO
          movies(popularity,
            vote_count,
            video,
            poster_path,
            adult,
            backdrop_path,
            original_language,
            original_title,
            genre_ids,
            title,
            vote_average,
            overview,
            release_date)
          VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
          returning *`;
  const values = [
    popularity,
    vote_count,
    video,
    poster_path,
    adult,
    backdrop_path,
    original_language,
    original_title,
    genre_ids,
    title,
    vote_average,
    overview,
    release_date,
  ];

  try {
    const { rows } = await dbQuery.query(createMovieQuery, values);
    const dbResponse = rows[0];
    successMessage.data = dbResponse;
    console.log('dbresponse:')
    console.log(dbResponse)

    return res.status(status.created).send(successMessage);
  } catch (error) {
    console.log(error);
    errorMessage.error = "Operation was not successful";
    return res.status(status.error).send(errorMessage);
  }
};

const getMovies = async (req, res) => {
  const limit = 10;
  let offset = 0;

  if (req.query.page) offset = (req.query.page - 1) * limit;
  const getAllBookingsQuery = `SELECT * FROM movies LIMIT ${limit} OFFSET ${offset}`;

  try {
    const { rows } = await dbQuery.query(getAllBookingsQuery);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      errorMessage.error = "There are no movies";
      return res.status(status.bad).send(errorMessage);
    }
    successMessage.data = dbResponse;
    return res.status(status.success).send(successMessage);
  } catch (error) {
    errorMessage.error = "An error Occured";
    return res.status(status.error).send(errorMessage);
  }
};

const searchMovie = async (req, res) => {
  const { query } = req.body;
  const getAllBookingsQuery = `SELECT * FROM movies WHERE LOWER(title) LIKE LOWER('%${query}%')`;

  try {
    const { rows } = await dbQuery.query(getAllBookingsQuery);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      errorMessage.error = "There are no movies";
      return res.status(status.bad).send(errorMessage);
    }
    successMessage.data = dbResponse;
    return res.status(status.success).send(successMessage);
  } catch (error) {
    console.log(error);
    errorMessage.error = "An error Occured";
    return res.status(status.error).send(errorMessage);
  }
};

export { addMovie, getMovies, searchMovie };
