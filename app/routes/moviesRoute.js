import express from 'express'

import {addMovie, getMovies, searchMovie} from '../controllers/moviesController'

const router = express.Router();

router.post('/addMovie', addMovie);
router.get('/getMovies', getMovies);
router.get('/searchMovie', searchMovie);

export default router;