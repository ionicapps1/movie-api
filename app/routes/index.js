import { Router } from 'express';
import movies from './moviesRoute';

const routes = Router();

routes.use('/movies', movies);

export default routes;